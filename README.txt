In order for the dmarc module to accept HTTP POST data, the following php.ini
settings must be in place

allow_url_fopen = On
allow_url_include = On
