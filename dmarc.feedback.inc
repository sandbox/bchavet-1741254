<?php
/**
 * @file
 * Provides functions to handle the feedback submission form.
 */

/**
 * Defines the file upload form to manually submit DMARC reports.
 */
function dmarc_feedback_form($form_state) {
  $form = array();

  $form['dmarc_report'] = array(
    '#title' => t('Upload'),
    '#type' => 'managed_file',
    '#title_display' => 'invisible',
    '#upload_location' => 'public://dmarc/',
    '#upload_validators' => array(
      'file_validate_extensions' => array()),
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Handle the submitted feedback form.
 */
function dmarc_feedback_form_submit(&$form, &$form_state) {

  if ($form_state['values']['dmarc_report']) {
    // Save the file.
    $file = file_load($form_state['values']['dmarc_report']);
    $file->status = FILE_STATUS_PERMANENT;
    file_save($file);

    // Record that this module is using the file.
    file_usage_add($file, 'dmarc', 'dmarc_report', 1);

    return dmarc_parse_file($file);
  }

}

/**
 * Handle DMARC feedback reports.
 */
function dmarc_feedback() {

  // Handle direct POST data.
  if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_SERVER['CONTENT_TYPE'] == 'application/zip') {

    // Extract the raw POST data.
    if ($stream = fopen('php://input', 'r')) {
      $data = stream_get_contents($stream);
      fclose($stream);
    }
    else {
      return FALSE;
    }

    // Save the POST data into a managed file.
    $file = file_save_data($data, 'public://dmarc/');

    // Set the file attributes.
    $file->filemime = 'application/zip';
    $file->status = FILE_STATUS_PERMANENT;

    // Save the file.
    file_save($file);

    // Record that this module is using the file.
    file_usage_add($file, 'dmarc', 'dmarc_report', 1);

    dmarc_parse_file($file);
  }
  // Otherwise, present a form to submit a DMARC report manually.
  else {
    return drupal_get_form('dmarc_feedback_form');
  }

}
